<?php
use Faker\Generator as Faker;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\Subscribe::class, function (Faker $faker) {
    return [
        'expiring_date' => $faker->dateTimeInInterval($date = 'now', $interval = '+5 days', $timezone = null),
    ];
});
