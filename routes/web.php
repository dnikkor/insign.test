<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@datatable');
Route::get('edit/customer/{id}/','HomeController@customer_edit');
Route::post('edit/customer/{id}/','HomeController@customer_save');