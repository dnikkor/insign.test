@extends('layouts.app')

@section('styles')
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        {{ Form::open(array('url' => '/edit/customer/'.$customer->id.'/')) }}
        <br>
        <div class="row">
            {{ Form::label('login', 'Login:', array('class' => 'col-sm-2')) }}
            {{ Form::text('login', $value = $customer->login, $attributes = array( 'class' => 'form-control  col-sm-3')) }}
        </div>
        <br>
        <div class="row">
            {{ Form::label('first_name', 'First name:', array('class' => 'col-sm-2')) }}
            {{ Form::text('first_name', $value = $customer->first_name, $attributes = array( 'class' => 'form-control col-sm-3')) }}
        </div>
        <br>
        <div class="row">
            {{ Form::label('name', 'Name:', array('class' => 'col-sm-2')) }}
            {{ Form::text('name', $value = $customer->name, $attributes = array( 'class' => 'form-control col-sm-3')) }}
        </div>
        <br>
        <div class="row">
            {{ Form::label('last_name', 'Last name:', array('class' => 'col-sm-2')) }}
            {{ Form::text('last_name', $value = $customer->last_name, $attributes = array( 'class' => 'form-control col-sm-3')) }}
        </div>
        <br>
        <div class="row">
            {{ Form::label('password', 'Password:', array('class' => 'col-sm-2')) }}
            {{ Form::password('password', $attributes = array( 'class' => 'form-control col-sm-3')) }}
        </div>
        <br>
        <div class="row">
            {{ Form::label('email', 'Email:', array('class' => 'col-sm-2')) }}
            {{ Form::email('email', $value = $customer->email, $attributes = array( 'class' => 'form-control col-sm-3')) }}
        </div>
        <br>
        <div class="row">
            {{ Form::label('expiring_date', 'Subscribe\'s expiring date:', array('class' => 'col-sm-2')) }}
            {{ Form::text('expiring_date', ($customer->subscribe['expiring_date'])? $value = date('d.m.Y', strtotime($customer->subscribe['expiring_date'])): '',$attributes = array( 'class' => 'date form-control col-sm-3') ) }}
        </div>
        <br>
        <div class="row">
            {{ Form::submit('Save', $attributes = array( 'class' => 'btn btn-primary')) }}
            <a class="btn btn-primary ml-2" href="/" role="button">Back to the list</a>
        </div>
        {{ Form::close() }}
    </div>

@endsection

@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script>
        $('.date').datepicker({
            format: 'dd.mm.yyyy'
        });
    </script>
@endsection
