<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Customer extends Model
{
    public function subscribe()
    {
        return $this->hasOne('App\Subscribe');
    }
}