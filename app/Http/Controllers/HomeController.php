<?php
namespace App\Http\Controllers;
use App\Customer;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function datatable()
    {
        $customers = Customer::with('subscribe')->get();
        return view('customers.datatable', compact('customers'));
    }

    public function customers()
    {
        $rawCustomers = Customer::with('subscribe')->get()->toArray();
        $customers = [];
        foreach ($rawCustomers  as $id => $rawCustomer)
        {
            $customers[$id]['id'] = $rawCustomer['id'];
            $customers[$id]['login'] = $rawCustomer['login'];
            $customers[$id]['fio'] = "{$rawCustomer['first_name']} {$rawCustomer['name']} {$rawCustomer['last_name']}";
            if($rawCustomer['subscribe'])
            {
                $customers[$id]['expiring_date'] = date('d-m-Y', strtotime($rawCustomer['subscribe']['expiring_date']));
            }
        }

        return json_encode($customers);
    }

    public function customerUpdate(Request $request, $id)
    {
        $rawCustomer = $request->all();
        $customer = Customer::with('subscribe')->findOrFail($id);
        (isset($rawCustomer['login']))? $customer->login = $rawCustomer['login'] : '';
        (isset($rawCustomer['first_name']))? $customer->first_name = $rawCustomer['first_name'] : '';
        (isset($rawCustomer['name']))? $customer->name = $rawCustomer['name'] : '';
        (isset($rawCustomer['last_name']))? $customer->last_name = $rawCustomer['last_name'] : '';
        (isset($rawCustomer['password']))? $customer->password = $rawCustomer['password'] : '';
        (isset($rawCustomer['email']))? $customer->email = $rawCustomer['email'] : '';
        if(isset($rawCustomer['expiring_date']))
        {
            $customer->subscribe()->updateorcreate(
                ['customer_id' => $id],
                ['expiring_date' => date('Y-m-d H:i:s', strtotime($rawCustomer['expiring_date'].'23:59:59'))]
            );
        }
        else
        {
            $customer->subscribe()->delete();
        }
        $customer->save();
        return 200;
    }

    public function customer_edit($id)
    {
        $customer = Customer::with('subscribe')->findOrFail($id);
        return view('customers.edit', compact('customer'));
    }

    public function customer_save($id)
    {
        $customer = Customer::with('subscribe')->findOrFail($id);
        $customer->login = $_POST['login'];
        $customer->first_name = $_POST['first_name'];
        $customer->name = $_POST['name'];
        $customer->last_name = $_POST['last_name'];
        if($_POST['password'])
        {
            $customer->password = $_POST['password'];
        }
        $customer->email = $_POST['email'];

        if($_POST['expiring_date'])
        {
            $customer->subscribe()->updateorcreate(
                ['customer_id' => $id],
                ['expiring_date' => date('Y-m-d H:i:s', strtotime($_POST['expiring_date'].'23:59:59'))]
            );
        }
        else
        {
            $customer->subscribe()->delete();
        }
        $customer->save();
        $customer = Customer::with('subscribe')->findOrFail($id);

        return View('customers.edit', compact('customer'));
    }
}