<?php

namespace App\Http\Middleware;

use Closure;

class ApiHttpBasicAuth
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $env = config('auth.apibasicauth');
        if($request->getUser() == $env['AUTH_LOGIN'] && $request->getPassword() == $env['AUTH_PASSWORD'])
        {
            return $next($request);
        } else {
            return response('Unauthorized.', 401);
        }
    }
}