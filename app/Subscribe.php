<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscribe extends Model
{
    protected $fillable = array('expiring_date');

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}